//
//  ControlCenter.swift
//  Pirate Fleet
//
//  Created by Jarrod Parkes on 9/2/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

struct GridLocation {
    let x: Int
    let y: Int
}

struct Ship {
    let length: Int
    let location: GridLocation
    let isVertical: Bool
}
struct Mine: _Mine_ {
    let location: GridLocation
    let explosionText: String
}

class ControlCenter {
    
    func addShipsAndMines(human: Human) {
        let smallShip = Ship(length: 2, location: GridLocation(x: 0,y: 0), isVertical: true)
        let mediumship1 = Ship(length: 3, location: GridLocation(x:2,y: 1), isVertical: false)
        let mediumShip2 = Ship(length: 3, location: GridLocation(x: 5,y: 1), isVertical: false)
        let largeShip = Ship(length: 4, location: GridLocation(x: 5,y: 3), isVertical: true)
        let x_largeShip = Ship(length: 5, location: GridLocation(x: 0,y: 6), isVertical: false)
        
        human.addShipToGrid(smallShip)
        human.addShipToGrid(mediumship1)
       human.addShipToGrid(mediumShip2)
        human.addShipToGrid(largeShip)
        human.addShipToGrid(x_largeShip)
        
        let mine1 = Mine(location: GridLocation(x: 7,y: 7), explosionText: "first mine")
        let mine2 = Mine(location: GridLocation(x: 0,y: 5), explosionText: "second mine")
        
        human.addMineToGrid(mine1)
        human.addMineToGrid(mine2)
        
    }
    
    func calculateFinalScore(gameStats: GameStats) -> Int {
        
        var finalScore: Int
        let enemyShipsSunk: Int = 5 - gameStats.enemyShipsRemaining
        let humanShipsRemaining: Int = 5 - gameStats.humanShipsSunk
        let numberOfGuesses: Int = gameStats.numberOfHitsOnEnemy + gameStats.numberOfMissesByHuman
        
        finalScore = (enemyShipsSunk * gameStats.sinkBonus) + (humanShipsRemaining * gameStats.shipBonus) - (numberOfGuesses * gameStats.guessPenalty)
        
        return finalScore
    }
}